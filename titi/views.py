from django.shortcuts import render, get_object_or_404
from titi.models import Fiche


def accueil(request):
    fiches = Fiche.objects.all()
    nb_fiches = fiches.count()
    nb_visibles = fiches.filter(visible=True).count()
    return render(
        request,
        "titi/accueil.html",
        {"total": nb_fiches, "visibles": nb_visibles}
    )


def fiche(request, fiche_id):
    # Simple mais pas bien
    fiche = Fiche.objects.get(id=fiche_id)
    # La bonne façon de faire
    fiche = get_object_or_404(Fiche, id=fiche_id)

    return render(
        request,
        "titi/fiche.html",
        {"fiche": fiche}
    )
