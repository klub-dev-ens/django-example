from django.db import models


class Fiche(models.Model):
    name = models.fields.CharField(max_length=1024)
    promo = models.fields.IntegerField()
    visible = models.fields.BooleanField()

    def __str__(self):
        return "Fiche de {} ({})".format(self.name, self.promo)
